#!/usr/bin/env node

/*****************************************
 * commando para popular o banco de dados
 * > mongo populadb.js <MONGO_URI>
 ****************************************/

console.log("Esse script irá criar algumas coleções em seu banco de dados!")

// pega o argumento passado pela linha de comando
var userArgs = process.argv.slice(2)

var async = require('async')
var {createHmac} = require('crypto')
var Usuario = require('./models/usuario')
var TipoDoacao = require('./models/tipoDoacao')
var Doacao = require('./models/doacao')
var NecesDoacao = require('./models/necesDoacao')
var Coleta = require('./models/coleta')

var mongoose = require('mongoose')
const doacao = require('./models/doacao')
const usuario = require('./models/usuario')
var mongoDB = userArgs[0]
mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true})
mongoose.Promise = global.Promise
var db = mongoose.connection
db.on('error', console.error.bind(console, 'Erro de conexão em MongoDB'))

var usuarios = []
var doacoes = []
var coletas = []
var tipodoacoes = []
var necesdoacoes = []

const usuarioCreate = (nome, tel, email, rua, num, cep, comp, cidade, estado, tipo, senha, cb) => {
    var hash = createHmac('sha256', senha).digest('hex')
    usuariodetail = {
        nome: nome,
        contatos: { telefone: tel, email: email },
        endereco: { rua: rua, numero: num, cep: cep, complemento: comp, cidade: cidade, estado: estado },
        senha: { hash: hash },
        tipo: tipo
    }

    var usuario = new Usuario(usuariodetail)
    
    usuario.save((err)=>{
        if(err){
            cb(err, null)
            console.log(err)
            return
        }
        console.log('Novo usuario: ' + usuario)
        usuarios.push(usuario)
        cb(null, usuario)
    })
}

const tipodoacaoCreate = (descricao, cb) => {
    var tipodoacao = new TipoDoacao({descricao: descricao})

    tipodoacao.save((err) =>{
        if(err){
            cb(err, null)
            return
        }
        console.log('Novo tipo de doacao: ' + tipodoacao)
        tipodoacoes.push(tipodoacao)
        cb(null, tipodoacao)
    })
}

const doacaoCreate = (usuario, tipo_doacao, qtd, data_coleta, cb) => {
    var doacaodetail = { 
        usuario: usuario,
        tipo_doacao: tipo_doacao,
        quantidade: qtd
    }

    if (data_coleta != false) doacaodetail.data_coleta = data_coleta

    var doacao = new Doacao(doacaodetail)

    doacao.save((err)=>{
        if(err){
            cb(err, null)
            return
        }
        console.log('Novo doacao: ' + doacao)
        doacoes.push(doacao)
        cb(null, doacao)
    })
}

const coletaCreate = (coletor, necessidade, doacao, data_coleta, data_entrega, cb) => {
    var coletadetail = {
        usuario: coletor,
        necessidade_doacao: necessidade,
        doacao: doacao       
    }

    if(data_coleta != false) coletadetail.data_coleta = data_coleta
    if(data_entrega != false) coletadetail.data_entrega = data_entrega

    var coleta = new Coleta(coletadetail)

    coleta.save((err)=>{
        if(err){
            cb(err, null)
            return
        }
        console.log('Novo coleta: ' + coleta)
        coletas.push(coleta)
        cb(null, coleta)
    })
}

const necesdodacaoCreate = (usuario, nome_animal, tipo_doacao, qtd, data_entrega, cb) => {
    var necesdoacaodetail = {
        usuario: usuario,
        nome_animal: nome_animal,    
        tipo_doacao: tipo_doacao,
        quantidade: qtd
    }
    if(data_entrega != false) necesdoacaodetail.data_entrega = data_entrega
    
    var necesdoacao = new NecesDoacao(necesdoacaodetail)

    necesdoacao.save((err)=>{
        if(err){
            cb(err, null)
            return
        }
        console.log('Novo necessidade de doação: ' + necesdoacao)
        necesdoacoes.push(necesdoacao)
        cb(null, necesdoacao)
    })
}

const createUsuarios = (cb) => {
    async.series([                
        (callback) => {
            usuarioCreate("Julema Carine Merces","41 98847-2931","christian_williams@fakegmail.com","FRANCISCO LANZARIM",993,9256800,"casa","curitiba","paraná","coletor","1234567",callback)
        },
        (callback) => {
            usuarioCreate("Megan Turner","41 98764-0982","megan_turner@fakegmail.com","FREDERICK DOUGLASS BLVD",44,8300902,"apartamento","S J Pinhais","Paraná","doador", "abc123",callback)
        },        
        (callback) => {
            usuarioCreate("Caroline paolla oliveira", "61 98765-4432","lolla@fakemail.com","barata ribeira",67,89921300,"casa","rio de janeiro","rio de janeiro","doador","akj456",callback)
        },
        (callback) => {
            usuarioCreate("Larissa de macedo machado","61 8876-2134","larissamacedo@fakemail.com","ribeirao jardim",06,456213,"boa vista","brasilia","destrito federal","doador","l2k3jjh4",callback)
        },
        (callback) => {
            usuarioCreate("Pablo J. santos","41 98762-1284","jota@fakemail.com","brasilio dal negro",130,83025780,"condominio","s j pinhais","paraná","padrinho","123abc",callback)
        },
        (callback) => {
            usuarioCreate("Otaviano mesquita","41 89021-0921","mesquita22@fakemail.com","comendador fontana",1000,89201209,"","piraquara","paraná","padrinho","a0a9d8f7",callback)
        },
        (callback) => {
            usuarioCreate("José ribas beneditto","41 98764-2198","jédobar@fakemail.com","jaime vaiga",42,89034218,"apartamento","curitiba","paraná","padrinho","98uyt2",callback)
        }
        
    ], cb)
}

const createTipodoacao = (cb) => {
    async.parallel([
        (callback) => {
            tipodoacaoCreate("ração", callback)
        },
        (callback) => {
            tipodoacaoCreate("xampu", callback)   
        }
    ], cb)
}

const createDoacao = (cb) => {
    async.parallel([
        (callback) => {
            doacaoCreate(usuarios[1], tipodoacoes[0], 5, "2021/04/18", callback)
        },
        (callback) => {
            doacaoCreate(usuarios[2], tipodoacoes[0], 10, "2021/04/02", callback)
        },
        (callback) => {
            doacaoCreate(usuarios[3], tipodoacoes[1], 7, "2021/04/29", callback)
        }
    ], cb)
}

const createNecesdoacao = (cb) => {
    async.parallel([
        (callback) => {
            necesdodacaoCreate(usuarios[4], "zazita", tipodoacoes[0], 5, "2021/05/08", callback)
        },
        (callback) => {
            necesdodacaoCreate(usuarios[4], "floquinho", tipodoacoes[0], 10, "2021/05/11", callback)
        },
        (callback) => {
            necesdodacaoCreate(usuarios[5], "pingo", tipodoacoes[1], 1, "2021/04/30", callback)
        }
    ], cb)
}


const createColeta = (cb) => {
    async.parallel([
        (callback) => {
            coletaCreate(usuarios[0], necesdoacoes[1], doacoes[0], "2021/04/18", "2021/05/11",callback)
        },
        (callback) => {
            coletaCreate(usuarios[0], necesdoacoes[0], doacoes[1], "2021/04/18", "2021/04/02", callback)
        },
        (callback) => {
            coletaCreate(usuarios[0], necesdoacoes[2], doacoes[2], "2021/04/30", "2021/05/02", callback)
        }
    ], cb)
}

async.series([
    createUsuarios,
    createTipodoacao,
    createDoacao,
    createNecesdoacao,
    createColeta

], (err, results) => {
    if (err) {
        console.log('FINAL ERR: '+err)
    }
    else {
        console.log('Ok')       
    }    
    mongoose.connection.close()
})