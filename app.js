const mongoose = require('mongoose')
const fs = require('fs')
const connectionString = JSON.parse(fs.readFileSync('./mongouri.json'))


/**************************************
 * CONEXÃO COM O BANCO DE DADO MONGODB
 **************************************/
const uri = connectionString.MONGO_URI
mongoose.connect(uri, {useNewUrlParser: true, useUnifiedTopology: true})
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB erro de conexão:'))


/****************************
 * IMPORTAÇÃO DOS CONTROLES 
 ****************************/
const usuario_controller = require('./controllers/usuarioController')
const doacao_controller = require('./controllers/doacaoController')
const tipodoacao_controller = require('./controllers/tipodoacaoController')
const necesdoacao_controller = require('./controllers/necesdoacaoController')
const coleta_controller = require('./controllers/coletaController')


/**********
 * USUARIO
 **********/
// Lista todos os usuarios de forma simples
//usuario_controller.usuario_list()  

// Lista todos usuarios de forma detalhada
//usuario_controller.usuario_detail()  

// Remove um usuario passando como parametro seu _id
//usuario_controller.usuario_delete("60d0a0019aa62f43e4ee5fe9")

// Cria um novo documento na coleção 'usuario' passando como parametros:
// Nome do usuário, rua, numero, cep, complemento, cidade, estado, email, telefone, senha, tipo
//usuario_controller.usuario_create("Megan Turner","FREDERICK DOUGLASS BLVD",44,8300902,"apartamento","S J Pinhais","Paraná","megan_turner@fakegmail.com","41 98764-0982","abc123","coletor")


/**********
 * DOAÇÃO
 **********/
// Lista dodas as Doações cadastradas mostrando apenas _id, tipo da doação e a quantidade
//doacao_controller.doacao_list()  

// Procurar doações por tipo de doação 
//doacao_controller.doacao_detail()  

// Cria uma nova Doação pasando o nome completo do usuario, tipo da doção, data de retirada e a quantidade
//doacao_controller.doacao_create("julema carine merces", "xampu", "2021/10/22", 9)  

// Deleta 'Doação' passando o ObjectId do documento
//doacao_controller.doacao_delete("60d0beb4edc1e51500db463f")  


/***************
 * TIPO DOAÇÃO
 ***************/
// Lista todos os documentos contidos na coleção 'TipoDoacao'
//tipodoacao_controller.tipodoacao_list()  

// Detalha as informações de um documento. Procura por tipo de doação
//tipodoacao_controller.tipodoacao_detail('ração') 

// Cria um novo documento na coleção 'TipoDoacao'
//tipodoacao_controller.tipodoacao_create("RACAO")

// Exclui um documento, passando como parametro o _id do documento
//tipodoacao_controller.tipodoacao_delete("60d0a0059aa62f43e4ee5ff8")


/**********************
 * NECESSIDADE DOAÇÃO
 **********************/
// Apresenta as informações de necessidade de doação de forma detalhada
//necesdoacao_controller.necesdoacao_detail()

// Exclui um documento, passando como parametro o _id do documento
//necesdoacao_controller.necesdoacao_delete("60d0d35018e9ca32e01c10f9")

// Cria um novo documento na coleção 'necessidadeDoacao' passando no parametro:
// nome do usuario, nome do animal, tipo doação, quantidadee data de entrega
//necesdoacao_controller.necesdoacao_create("pablo j. santos", "bagueira", "ração", 50, "1995/12/05")


/**********
 * COLETA
 **********/
// Lista todos as coletas de forma detalhada
//coleta_controller.coleta_detail()

// Cria um novo documento na coleção coleta passando como parametro, _id necessidade doação, _id doacao e o _id do coletor
//coleta_controller.coleta_create("60ce3d060a21af0a643a439d", "60cd552027c1053dd8e68ef3" ,"60ce65a75e08aa2b18b87941")