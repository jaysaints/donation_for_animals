const mongoose = require('mongoose')
const async = require('async')
const {DateTime} = require('luxon')
const NecesDoacao = require('../models/necesDoacao')
const Usuario = require('../models/usuario')
const TipoDoacao = require('../models/tipoDoacao')

exports.necesdoacao_detail = () => {
    NecesDoacao
        .find()
        .populate('usuario')
        .populate('tipo_doacao')
        .exec((err, res) => {
            if(err) {onErr(err) }
            res.forEach(element => {
                var msg = {'[+] Necessidade de Doação': { 
                        'ID ': element._id,
                        'Nome ': element.usuario.nome,
                        'Nome animal': element.nome_animal,
                        'Tipo doação ': element.tipo_doacao.descricao,
                        'Quantidade ': element.quantidade,
                        'Telefone ': element.usuario.contatos.telefone,
                        'Email ': element.usuario.contatos.email,
                        'Endereço ': {
                            'Rua ': element.usuario.endereco.rua,
                            'Num ': element.usuario.endereco.numero,
                            'Cep ': element.usuario.endereco.cep,
                            'Comp ': element.usuario.endereco.complemento,
                            'Cidade ': element.usuario.endereco.cidade,
                            'Estado ': element.usuario.endereco.estado
                        },
                        'Data entrega ': DateTime.fromJSDate(element.data_entrega).toLocaleString(DateTime.DATE_MED)
                    }
                } 
                console.log(msg)                
            })            
        })
}


exports.necesdoacao_create = (nome_user, nome_animal, tipo_doacao, qtd, data) => {    
    async.parallel({
        usuario: (callback) => {
            Usuario
                .findOne({"nome": nome_user})
                .exec(callback)
        },
        tipodoacao: (callback) => {
            TipoDoacao
                .findOne({"descricao": tipo_doacao})
                .exec(callback)
        }
    }, (err, results) => {
        if(err) { onErr(err) }
        
        if (results.usuario==null || results.tipodoacao==null){
            var err = new Error('Algo deu Errado')
            onErr(err)
        }               
        const usuario_id = mongoose.Types.ObjectId(results.usuario._id)
        const tipo_doacao_id = mongoose.Types.ObjectId(results.tipodoacao._id)
        NecesDoacao.create({
            "usuario": usuario_id,
            "nome_animal": nome_animal,
            "tipo_doacao": tipo_doacao_id,
            "quantidade": qtd,
            "data_entrega": data
        }, (err, res) => {
            if(err){ onErr(err) }
            console.log(`
                Necessidade cadastrada: 
                Identificador: ${res._id}
                Usuario: ${results.usuario.nome}
                Tipo doação: ${results.tipodoacao.descricao}
                Quantidade: ${res.quantidade}
                `)
        })
    })
}

exports.necesdoacao_delete = (id) => {
    const objId = mongoose.Types.ObjectId(id)
    const tt = NecesDoacao.deleteOne({"_id": objId}, (err, res)=> {
        if(err){onErr(err)}
        if(res.deletedCount != 0){
            console.log("Necessidade de doação removida! "+id)
        }
    })    
}



const onErr = (err) => {
    console.log(err)
    return 1
}