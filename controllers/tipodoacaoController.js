const mongoose = require('mongoose')
const TipoDoacao = require('../models/tipoDoacao')
const async = require('async')



exports.tipodoacao_list = () => {    
    async.parallel({
        tipodoacao: (callback) => {
            TipoDoacao
                .find()
                .exec(callback)   
        }
    }, (err, results) => {
            if(err) { onErr(err) }
            if (results.tipodoacao==null) { 
                var err = new Error('Tipo Doaçao não encontrado');
                onErr(err)            
            }
            console.log('\n[+] Tipo de Doações:')
            results.tipodoacao.forEach(element => {
                console.log({'Descrição': element.descricao })
           })            
    })
}

exports.tipodoacao_detail = (desc) => {    
    async.parallel({
        tipodoacao: (callback) => {
            TipoDoacao
                .find({'descricao': desc})
                .exec(callback)   
        }
    }, (err, results) => {
            if(err) { onErr(err) }
            if (results.tipodoacao==null) { 
                var err = new Error('Tipo Doaçao não encontrado');
                onErr(err)            
            }
            
            results.tipodoacao.forEach(element => {
                console.log({'Tipo de Doção':{
                    "ID ": element._id,
                    "Descrição ": element.descricao  
                } })
           })            
    })
}

exports.tipodoacao_create = (descricao) => {
    TipoDoacao.create({
        "descricao": descricao
    }, (err, res) => {
        if(err){ onErr(err) }
        console.log(res)
    })
}

exports.tipodoacao_delete = (objId) => {
    const id = mongoose.Types.ObjectId(objId)    
    TipoDoacao.findByIdAndDelete(id, (err, result) =>{
        if(err) { onErr(err) }
        const descricao = 
        console.log(`[-] ${result} removido`)
    })
}

const onErr = (err) => {
    console.log(err)
    return 1
}