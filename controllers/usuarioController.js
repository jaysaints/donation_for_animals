const mongoose = require('mongoose')
const async = require('async')
const {createHmac} = require('crypto')

const Usuario = require('../models/usuario')

// Lista os usuarios existentes
exports.usuario_list = async ()=>{
    Usuario
        .find()
        .sort([["nome", "ascending"]])
        .select("_id nome tipo")
        .exec((err, usuarios) => {
            if (err) { onErr(err) }                       
            usuarios.forEach(element => {
                console.log({'[+] Usuário': element})
            })
        })
}

// Mostra todas as informações de um usuario
exports.usuario_detail = ()=>{    
    async.parallel({
        usuario: (callback) => {
            Usuario
                .find()                
                .exec(callback)
        }
    }, (err, results) => {
        if(err) { onErr(err) }
        if (results.usuario==null) {
            var err = new Error('Usuario não encontrado');
            onErr(err)            
        }
        results.usuario.forEach(element => {
            console.log({'[+] Informações do Usuário': element})
        })                
    })
}

// Cria um novo documento na coleção 'usuarios'
exports.usuario_create = (nome, rua, num, cep, comp, cidade, estado, email, tel, senha, tipo, role) => {
    const hash = createHmac('sha256', senha).digest('hex')
    Usuario.create({
        "nome": nome,
        "endereco": {
            "rua": rua,
            "numero": num,
            "cep": cep,
            "complemento": comp,
            "cidade": cidade,
            "estado": estado
        },
        "contatos": {
            "email": email,
            "telefone": tel
        },
        "senha": {
            "hash": hash,
            "role": role
        },
        "tipo": tipo

    }, (err, result) => {
        if(err) { onErr(err)}
        console.log(result)
        console.log('[+] Novo Usuario!')
    })
}

// Deleta um documento da coleção 'usuarios'
exports.usuario_delete = (user_id) => {
    const id = mongoose.Types.ObjectId(user_id)
    
    Usuario.findByIdAndDelete(id, (err, res) => {
        if (err) { onErr(err) }
        console.log({'[-] Usuario Removido': res})
    })       
}


const onErr = (err)=>{
    console.log(err)
    return 1
}