const mongoose = require('mongoose')
const async = require('async')
const { DateTime } = require('luxon')
const Coleta = require('../models/coleta')
const Doacao = require('../models/doacao')
const Usuario = require('../models/usuario')
const NecesDoacao = require('../models/necesDoacao')


exports.coleta_detail = () => {
    async.parallel({
        coleta: (callback) => {
            Coleta
                .find()
                .populate('tipo_doacao')                               
                .exec(callback)
        }
    }, (err, results) => {
        results.coleta.forEach(async element => {
            //console.log(element.usuario.nome)
            var necUserId = mongoose.Types.ObjectId(element.necessidade_doacao)
            var doaUserId = mongoose.Types.ObjectId(element.doacao)
            var colUserId = mongoose.Types.ObjectId(element.usuario)
            
            
            var necUser = await NecesDoacao.findById(necUserId).populate('usuario')
            var doaUser = await Doacao.findById(doaUserId).populate('usuario')
            var colUser = await Usuario.findById(colUserId)

            var coletaInfos = {
                '[~] COLETOR': {
                    'nome': colUser.nome,
                    'Telefone': colUser.contatos.telefone
                },
                '[~] DOADOR': {
                    'nome': doaUser.usuario.nome,
                    'Telefone': doaUser.usuario.contatos.telefone,
                    'Email': doaUser.usuario.contatos.email,
                    'Rua': doaUser.usuario.endereco.rua,
                    'Num': doaUser.usuario.endereco.numero,
                    'Cep': doaUser.usuario.endereco.cep,
                    'Comp': doaUser.usuario.endereco.complemento,
                    'Cidade': doaUser.usuario.endereco.cidade,
                    'Estado': doaUser.usuario.endereco.estado 
                },
                '[~] Necessidade': {
                    'nome': necUser.usuario.nome,
                    'Telefone': necUser.usuario.contatos.telefone,
                    'email': necUser.usuario.contatos.email,
                    'Rua': necUser.usuario.endereco.rua,
                    'Num': necUser.usuario.endereco.numero,
                    'Cep': necUser.usuario.endereco.cep,
                    'Comp': necUser.usuario.endereco.complemento,
                    'Cidade': necUser.usuario.endereco.cidade,
                    'Estado': necUser.usuario.endereco.estado 
                },
                'Data coleta': DateTime.fromJSDate(element.data_coleta).toLocaleString(DateTime.DATE_MED),
                'Data entrega': DateTime.fromJSDate(element.data_entrega).toLocaleString(DateTime.DATE_MED)              
            }
            console.log("|=================================================================================|")  
            console.log(coletaInfos)            
        })
        //console.log(results)
    })
}

exports.coleta_create = (necesdoacao, doacao, coletor) => {
    const necesdoacao_id  = mongoose.Types.ObjectId(necesdoacao)
    const doacao_id = mongoose.Types.ObjectId(doacao)
    const usuario_id = mongoose.Types.ObjectId(coletor)

    async.parallel({
        find_coleta: (callback) => {
            Coleta
                .find()
                .populate("usuario")
                .exec(callback)
        }
    }, (err, results) => {
        if(err){onErr(err)}
        if (results.find_coleta==null) {
            console.log("hello")              
        }
        
        Coleta.create({
            "doacao": doacao_id,
            "necessidade_doacao": necesdoacao_id,
            "usuario": usuario_id
        }, (err, res)=>{
            console.log(res)
        })
    })
}


const onErr = (err) => {
    console.log(err)
    return 1
}