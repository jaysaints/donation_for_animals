const mongoose = require('mongoose')
const async = require('async')
const { DateTime } = require("luxon")
const Doacao = require('../models/doacao')
const TipoDoacao = require('../models/tipoDoacao')
const Usuario = require('../models/usuario')


exports.doacao_list = () => {
    Doacao
        .find()
        .populate('tipo_doacao')
        //.select('usuario tipo_doaco')
        .exec((err, doacao) => {
            if(err) { onErr(err) }
            doacao.forEach(element => {
                //console.log(`ID: ${element._id} | Tipo Doação: ${element.tipo_doacao.descricao}`)    
                console.log('[+] Doação: ',{
                    'ID ': element._id,
                    'Tipo doação': element.tipo_doacao.descricao,
                    'Quantidade ': element.quantidade
                })
            });
            
        })
}

exports.doacao_detail = (desc) => {
    async.parallel({
        doacao: (callback) => {
            Doacao
                .find()                            
                .populate('usuario')                
                .populate('tipo_doacao')
                .exec(callback)
        }
    }, (err, results) => {
        if(err) { onErr(err) }
        if (results.doacao==null) {
            var err = new Error('Algo esta errado');
            onErr(err)                       
        }         
        results.doacao.forEach(element => {
            var msg = {'[+] Doação': { 
                'ID ': element._id,
                'Doador(a) ': element.usuario.nome,
                'Tipo Doação ': element.tipo_doacao.descricao,
                'Quantidade ': element.quantidade,
                'Telefone ': element.usuario.contatos.telefone,
                'Email ': element.usuario.contatos.email,
                'Endereço ': {
                    'Rua ': element.usuario.endereco.rua,
                    'Num ': element.usuario.endereco.numero,
                    'Cep ': element.usuario.endereco.cep,
                    'Comp ': element.usuario.endereco.complemento,
                    'Cidade ': element.usuario.endereco.cidade,
                    'Estado ': element.usuario.endereco.estado
                },
                'Data Coleta ': DateTime.fromJSDate(element.data_coleta).toLocaleString(DateTime.DATE_MED)}
            }            
            if(desc!=null){ 
                if (element.tipo_doacao.descricao == desc) {
                    console.log(msg)
                }
            }else{
                console.log(msg)
            }   
        })     
    })
}

exports.doacao_create = (nome_user, tipo_doacao, data, qtd) => {  
    async.parallel({
        usuario: (callback) => {
            Usuario
                .findOne({"nome": nome_user})
                .exec(callback)
        },
        tipodoacao: (callback) => {
            TipoDoacao
                .findOne({"descricao": tipo_doacao})
                .exec(callback)
        }
    }, (err, results) => {
        if(err) { onErr(err) }
        if (results.usuario==null || results.tipodoacao==null) {
            var err = new Error('Verifique se o nome do Usuario ou o tipo da doação estão corretos');
            onErr(err)                       
        }
    
        const nome_user_id = mongoose.Types.ObjectId(results.usuario._id)
        const tipo_doacao_id = mongoose.Types.ObjectId(results.tipodoacao._id)
        Doacao.create({
            "usuario": nome_user_id,
            "tipo_doacao": tipo_doacao_id,
            "quantidade": qtd,
            "data_coleta": data
        }, (err, res) => {
            if(err){onErr(err)}
            console.log("Doação Registrada " + res)
        })
    })
}

exports.doacao_delete = (doacao_id) => {
    const objId = mongoose.Types.ObjectId(doacao_id)
    Doacao.deleteOne({'_id': objId}, (err, res) => {
        if(err){ onErr(err) }
        console.log(`[-] Doação deletada: ${objId}`)
    })
}

const onErr = (err) => {
    console.log(err)
    return 1
}