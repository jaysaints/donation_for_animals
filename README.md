Autor: Pablo J. Santos --> @_jaysaints

# Doações para Animais

Este projeto simula um sistema para uma instituição de caridade que promove a adoção de animais, o sistema tem a seguinte finalidade:
    - Manter o cadastro dos usuarios;
    - Manter os registros das doações;
    - Manter os registros das necessidades de doação;
    - Manter os registros das coletas e entregas;

Requisitos:
- Instalar o [nodejs](https://www.mongodb.com/try/download/community) 14.x.x
- Ter uma instancia do [MongoDB](https://www.mongodb.com/try/download/community)

1. Realize o clone do repositorio ou baixe o aquivo compactado [.zip](https://gitlab.com/jaysaints/donation_for_animals/-/archive/master/donation_for_animals-master.zip):
> `git clone`

2. Instalar as dependencias:
> `npm install`

3. Configurar o arquivo __mongouri.json__ para realizar a conexão com o banco de dados. No campo "MONGO_URI" coloque a _Connetion String_ do MongoDB:  
__Obs.:__ Coloque a URI dentro de aspas duplas.  
Ex.:
> `"MONGO_URI": "mongodb+srv://<minha_instancia_mongodb>/<nome_banco_de_dados>?retryWrites=true&w=majority"`

E no campo "MONGO_DB" informe o nome do seu banco de dados:  
 Ex.: 
> `"MONGO_DB": "<nome_banco_de_dados>"`

4. Realize a população de dados. No campo 'minha_instancia_mongodb' informe a URI do MongoDB, observe se o nome do banco de dados na string de conexão é o mesmo que você deseja popular:
> `node populadb.js "mongodb+srv://<minha_instancia_mongodb>/<meu_banco_de_dados>?retryWrites=true&w=majority"`

5. E por fim start a aplicação com o comando:
> `npm start`

6. Para executar as funções é necessário descomentar apenas a linha que deseja utilizar e depois salvar o arquivo. Obs.: O comando acima deve esta em execução.

7. Os resultados são apresentados em console.

  
  _Aqui fica o agradecimento de um entusiasta da programação!!!_   


