const mongoose = require('mongoose')
const Schema = mongoose.Schema

const TipoDoacao = new Schema({
    descricao: {
        type: String, 
        lowercase: true, 
        enum: ["ração", "racao", "xampu"]
    }
})

module.exports = mongoose.model('TipoDoacao', TipoDoacao)