const mongoose = require('mongoose')
const Schema = mongoose.Schema

const ColetaSchema = new Schema({
    doacao: { type: Schema.Types.ObjectId, ref: "Doacao", requered: true },
    necessidade_doacao: { type: Schema.Types.ObjectId, ref: "NecessidadeDoacao", requered: true },
    usuario: { type: Schema.Types.ObjectId, ref: "Usuario", requered: true },
    data_coleta: { type: Date },
    data_entrega: { type: Date }
})
module.exports = mongoose.model('Coleta', ColetaSchema)