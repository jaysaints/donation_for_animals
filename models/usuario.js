const mongoose = require('mongoose')
const Schema = mongoose.Schema;

const UsuarioSchema = new Schema({
    nome: {type: String, lowercase: true, maxLength: 100, required: true},
    endereco: {
        rua: { type: String, lowercase: true },
        numero: { type: Number, lowercase: true },
        cep: { type: Number, minLength: 3 },
        complemento: { type: String, lowercase: true },
        cidade: { type: String, lowercase: true },
        estado: { type: String, lowercase: true }
    },
    contatos: {
        email: { type: String, lowercase: true },
        telefone: { type: String }
    },
    senha: {
        hash: { type: String },
        role: { type: Number, enum: [1, 2], default: 2 }
    },
    tipo: { type: String, lowercase: true, enum: ["doador", "coletor", "padrinho"]},
    dataCriacao: { type: Date, default: Date.now}
})

module.exports = mongoose.model('Usuario', UsuarioSchema)