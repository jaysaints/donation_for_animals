const mongoose = require('mongoose')
const Schema = mongoose.Schema

const DoacaoSchema = new Schema({
    usuario: { type: Schema.Types.ObjectId, ref: "Usuario", required: true },
    tipo_doacao: { type: Schema.Types.ObjectId, ref: "TipoDoacao", required: true },
    quantidade: { type: Number, min: 1},
    data_coleta: { type: Date}
})

module.exports = mongoose.model('Doacao', DoacaoSchema)