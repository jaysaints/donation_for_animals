const mongoose = require('mongoose')
const Schema = mongoose.Schema

const NecessidadeDoacaoSchema = new Schema({
    usuario: { type: Schema.Types.ObjectId, ref: "Usuario", required: true },
    nome_animal: { type: String, lowercase: true, maxLength: 100 },
    tipo_doacao: { type: Schema.Types.ObjectId, ref: "TipoDoacao", required: true },
    quantidade: { type: Number, min: 1 },
    data_entrega: { type: Date}
})

module.exports = mongoose.model('NecessidadeDoacao', NecessidadeDoacaoSchema)